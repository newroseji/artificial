# README #

Artificial - a Python code for RaspberryPi based car/tank

### What is this repository for? ###

* This is the repo for RaspberryPi based artificial intelligent car/tank whatever you make out of it.

### How do I get set up? ###

* You need the servo motors
* RaspberryPi (any model)
* Python knowledge
* Some wires

### Contribution guidelines ###

* If you are interested to make it better, you are welcome
* It is python based fun!

### Who do I talk to? ###

* Niraj Byanjankar @newroseji